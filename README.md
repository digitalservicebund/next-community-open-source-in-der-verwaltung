# Herzlich Willkommen bei der NExT Community "Open Source in der Verwaltung"

Open Source und die Nachnutzung von Software sind in der Verwaltung keine Unbekannten mehr. Mit Maßnahmen wie dem Online-Zugangsgesetz und dem Servicestandard hat Open Source thematisch bereits Einzug in die Verwaltung gehalten. Dennoch zeigt die Praxis, dass es noch ein weiter Weg ist, bis Open-Source wirklich gelebt wird und seine Vorteile in der Verwaltung voll entfalten kann.

### Warum Open Source in der Verwaltung?

Unsere Community soll ein Katalysator sein für den Wandel zu einer Verwaltung, die Open-Source lebt. Denn Open Source ist mehr als nur eine Technologie; es ist eine Philosophie, die auf Zusammenarbeit, Transparenz und gemeinschaftlicher Innovation basiert. In der Verwaltung bietet Open Source die Möglichkeit, Prozesse zu optimieren, die Effizienz zu steigern und eine Kultur des Teilens und der Zusammenarbeit zu fördern. Durch die Nutzung von Open Source können wir sicherstellen, dass öffentliche Gelder effektiv eingesetzt werden, um Lösungen zu schaffen, die allen zugutekommen.

### Was macht unsere Community einzigartig?

Unsere Community ist ein lebendiges Ökosystem, in dem Mitglieder Wissen austauschen, Erfahrungen teilen und Best Practices im Bereich der Open-Source-Nutzung in der Verwaltung diskutieren. Durch regelmäßige (virtuelle) Treffen bieten wir eine Plattform für den Dialog und die Zusammenarbeit, um technische, rechtliche und organisatorische Herausforderungen bei der Implementierung von Open Source zu meistern. Durch Networking entstehen neue Verbindungen mit Gleichgesinnten und Fachexperten, die zu zukünftigen Kooperationen führen können.

### Deine Vorteile als Mitglied

Als Mitglied von "Open Source in der Verwaltung" profitierst du von einer Fülle von Ressourcen und Möglichkeiten:

**Wissensaustausch:** Erhalte Zugang zu einem reichen Pool an Wissen und Erfahrungen, der dir hilft, die besten Open-Source-Praktiken in deiner Organisation umzusetzen.

**Networking:** Vernetze dich mit Gleichgesinnten und Experten, die deine Leidenschaft für Open Source und Innovation teilen.

**Lösungsfindung:** Arbeite gemeinsam an der Überwindung von Herausforderungen und nutze die kollektive Intelligenz der Community, um praktikable Lösungen zu entwickeln.

### Werde Teil unserer Community

Wir laden dich herzlich ein, Teil unserer wachsenden Community zu werden. Egal, ob du ein erfahrener IT-Profi bist oder einfach nur Interesse an der Zukunft der öffentlichen Verwaltung hast, deine Perspektive und dein Beitrag sind wertvoll. Gemeinsam können wir eine Verwaltung schaffen, die offen, effizient und bereit für die Herausforderungen der Zukunft ist.

### Mach den ersten Schritt

Interessiert? [Dann abboniere jetzt unseren Email Verteiler](https://verteiler.next-netz.de/wws/subscribe/cop-opensource?previous_action=info) und erhalte Updates und Einladungen zu unseren regelmäßigen (virtuellen) Treffen. 



Liebe Grüße

euer Community-Orga Team [(Janou, Leia & Nicolai)](https://next-netz.de/communities/open-source-in-der-verwaltung/)


|                       |                      |
|-----------------------|------------------------|
| Archiv der Veranstaltungen | [Archiv](https://gitlab.opencode.de/next-netz/next-community-open-source-in-der-verwaltung/-/blob/main/Unsere%20Community%20Treffen.md) |
| Unsere Internetseite: |  [Open-Source in der Verwaltung](https://next-netz.de/communities/open-source-in-der-verwaltung/) | 
| Unser Chat-Tool: | [RuDi](https://rudi.rvr.ruhr/toro/resource/html#/homescreen/newscover) | 
| Anmeldung für unser Mailingliste: | [Mailingliste](https://verteiler.next-netz.de/wws/subscribe/cop-opensource?previous_action=info) | 


