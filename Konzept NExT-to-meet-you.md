## NExT-to-meet-you

Wann: 15.05.2025
Dauer: 45 Minuten

Zum Format: Bei den Workshops soll es nicht um die Arbeit der Communities bzw. Leitungen gehen, sondern um einen Workshop zu einem Thema eurer Community, aus dem die Teilnehmenden im besten Fall etwas für ihre eigene Arbeit mitnehmen können.

Zielgruppe: Breite Zielgruppe, komplett gemischt

Deadline: Keine Deadline, aber in den nächsten Wochen sollte zu oder abgesagt werden bzgl. der Workshop Einreichung.


## Workshop-Ideen 

### Workshop Idee 1 "Der Allgemeine": "Open Source 101 – Was ist das und warum ist es wichtig für die Verwaltung?"
- Einführung in die Grundlagen von Open Source: Definition, Lizenzen, Community.
- Vorstellung der wichtigsten Vorteile: Kosteneinsparungen, Unabhängigkeit, Transparenz, Sicherheit, Innovation.
- Beispiele erfolgreicher Open-Source-Anwendungen in der Verwaltung (national und international).
- Diskussion: Wie können diese Vorteile konkret in unserer Verwaltung genutzt werden?

### Workshop Idee 2 "Der Praxisnahe": "Open Source in der Praxis – Erfolgreiche Projekte und Lösungen"
- Vorstellung konkreter Open-Source-Projekte und -Lösungen, die in der Verwaltung eingesetzt werden können (z.B. im Bereich Geodaten, Dokumentenmanagement, Webanwendungen).
- Expertenbericht: Vertreter:in von der Verwaltung, die bereits erfolgreich Open Source einsetzt, berichtet über ihre Erfahrungen.
- Live-Demonstrationen: Vorführung ausgewählter Open-Source-Software.
- Frage- und Antwortrunde: Möglichkeit, Fragen zu stellen und sich auszutauschen.

### Workshop Idee 3 "Der Aktivierende" : Open-Source: Was kann ich konkret machen?

1. Einführung: Warum Open Source?

- Kurze Vorstellung des Open-Source-Konzepts
- Vorteile von Open Source (Transparenz, Sicherheit, Kostenersparnis, etc.)
- Beispiele für erfolgreiche Open-Source-Projekte

2.  Möchte ich Software selbst entwickeln oder bestehende Software ersetzen?

- Diese Frage hilft den Teilnehmern, ihren Fokus zu finden.
- Klärung der unterschiedlichen Anforderungen und Herausforderungen

3. Produktentwicklungsebene

- Software selbst entwickeln:
    -  Wie kann ich nach Open-Source-Prinzipien entwickeln?
    -   Lizenzmodelle
    - In der Öffentlichkeit Arbeiten: Kollaboratives Arbeiten (Tools und - Plattformen wie OpenCode)
- Bestehende Software ersetzen:
    - Identifizierung von Open-Source-Alternativen
    - Migrationsprozesse und -herausforderungen
    - Support und Community-Aspekte

4. Politische Ebene

- Wie kann man in der Verwaltungsrealität das Open-Source-Thema stärken?
- Praktische Tools und Strategien
- Thema: Vergabe (rechtliche Rahmenbedingungen, Open-Source-Kriterien in Ausschreibungen)


