## Ablauf Treffen

Wir treffen uns regelmäßig zum (virtuellen) Austausch. Die Treffen bestehen in der Regel aus einem Impulsvortrag und der anschließenden Diskussion. 

Dauer: 1 - 1.5h  
Frequenz: Mindestens 1 mal im Quartal  

### Nächstes Treffen 

NExT-to-meet-you (15. Mai) -> Kick-off

24. Juni 11:00 45-Minuten-Session zur Vorstellung der OS Community als "BYOC (Build your own Community): Open Source Community Kick-off" - wir planen gemeinsam die nächsten Treffen und Community-Aktivitäten, indem wir uns kennenlernen und relevante Inhalte brainstormen.

26. Juni, 11:00 - 12:00: Beschaffung Open Source mit OSBA?

28. Oktober, 11:00 - 12:00: Gesprächsrunde der OS-Leuchttürme (Dortmund, München, Berlin?)

11. xx xxxx, 11:00 - 12:00: Abhängig von Themenwünsche der Community: Projektvorstellung mit Fokus auf der Praxis (z.B. GA Lotse, https://bva.usercontent.opencode.de/softwaretest/qs-baukasten/, ArchRL?); Impulsvortrag + Fragenrunde mit einem Juristen zu rechtlichen Chancen und Hürden von OSS?




### Bisherige Treffen

#### 17.09.2025 Thema: OZG Hub: Vorteile von OpenSource Lösungen für die Verwaltung, sowie für die Softwareentwicklung
Speaker: Michael Schmid (Seitenbau), Dr. Holger Schäfers (Innenministerium Baden-Württemberg)
Ressourcen: TBD

#### 27.02.2024 Thema: Überblick über die Fortschritte von FLOSS in der öffentlichen Verwaltung
Speaker: Marco Holz (FITKO)  
Ressourcen: [Vortragsvideo](https://fosdem.org/2024/schedule/event/fosdem-2024-2818-some-updates-on-public-code-in-germany/)


#### 19.12.2023 Thema: Open Source Lizenzen  
Speaker: Claus Wickinghoff  
Ressourcen: [Vortragsfolien](next_2023_01.pdf)


#### 18.10.2023 Community Kick-Off
Speaker: Andreas, Christian und Nicolai  
Ressourcen: [Vortragsfolien](Kick-Off_NExT_Community__Open_Source_in_der_Verwaltung_.pdf)


